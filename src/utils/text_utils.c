#include "text_utils.h"
#include "stdio.h"
#include <stdio.h>
#include <stdlib.h>


long get_file_size(const char *filename)
{
  FILE *file = fopen(filename, "r");
  if (file == NULL)
  {
    return -1;
  }

  long size = 0;
  while (getc(file) > 0)
  {
    ++size;
  }

  fclose(file);

  return size;
}


char *get_file_text_allocated(const char *filename)
{
  long size = get_file_size(filename);
  if (size <= 0)
  {
    return NULL;
  }

  char *text = (char *)malloc(sizeof(char) * size + 1);
  if (text == NULL)
  {
    return NULL;
  }

  FILE *file = fopen(filename, "r");
  if (file == NULL)
  {
    free(text);
    return NULL;
  }

  for (int i = 0; i < size; ++i)
  {
    text[i] = getc(file);
  }
  text[size] = '\0';

  fclose(file);

  return text;
}
