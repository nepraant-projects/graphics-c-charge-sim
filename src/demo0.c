#include <GL/glew.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "geometry/3f/vec3f.h"
#include "geometry/geometry.h"
#include "graphics/camera.h"
#include "graphics/object_buffer.h"
#include "physics/particles/particle.h"
#include "physics/particles/particle_sim.h"
#include "physics/physics.h"
#include "physics/verlets/verlet3f.h"
#include "utils/text_utils.h"

#define VS_FILENAME "src/shaders/vs.glsl"
#define FS_FILENAME "src/shaders/fs.glsl"


int window_w = 1200;
int window_h = 1000;

GLint gObjectTransform;
GLint gCameraTransform;

GLuint VBO;

long vertex_count;

unsigned red_ball_pos;
unsigned red_ball_size;
unsigned green_ball_pos;
unsigned green_ball_size;
unsigned blue_ball_pos;
unsigned blue_ball_size;

vec3f_t cam_pos = (vec3f_t){0, 40, 40};

float cam_rot_x = 0;
float cam_rot_y = 0;

float t = 0.01;

particle_sim_t *sim;
camera_t *cam;

void mat4f_to_buffer(mat4f_t m, float buffer[16])
{
  for (int i = 0; i < 4; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      buffer[4 * i + j] = m.data[j][i];
    }
  }
}

void drawObject(unsigned buffer_pos, unsigned buffer_size, vec3f_t position)
{
  mat4f_t transformation =
      mat4f_translate_xyz(position.x, position.y, position.z);

  float matrix_buffer[16];
  mat4f_to_buffer(transformation, matrix_buffer);
  glUniformMatrix4fv(gObjectTransform, 1, GL_FALSE, matrix_buffer);
  glDrawArrays(GL_TRIANGLES, buffer_pos / 3, buffer_size / 3);
}

void drawObjectScaled(unsigned buffer_pos, unsigned buffer_size,
                      vec3f_t position, float scale)
{
  mat4f_t transformation =
      mat4f_mul_mat4f(mat4f_translate_xyz(position.x, position.y, position.z),
                      mat4f_scale(scale));

  float matrix_buffer[16];
  mat4f_to_buffer(transformation, matrix_buffer);
  glUniformMatrix4fv(gObjectTransform, 1, GL_FALSE, matrix_buffer);
  glDrawArrays(GL_TRIANGLES, buffer_pos / 3, buffer_size / 3);
}

int prepare_sim()
{
  vec3f_t v0 = vec3f_init(-20, -20, -20);
  bool inv = false;
  for (int i = 0; i < 8; ++i)
  {
    for (int j = 0; j < 8; ++j)
    {
      for (int k = 0; k < 8; ++k)
      {

        vec3f_t v =
            vec3f_add(v0, vec3f_mul(vec3f_init(i, j, k),
                                    5 + rand() / (float)RAND_MAX * 0.1));
        particle_sim_add_particle(sim,
                                  particle_create(v, 1, 1, inv ? -1E-4 : 1E-4));
      }
      inv = !inv;
    }
  }

  return 0;
}


void renderScene()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /*
   static int counter0 = 0;
   static int counter1 = 40;

   if (counter0 > 80)
   {
     float x = rand() / (float)RAND_MAX * 20 - 10;
     float y = rand() / (float)RAND_MAX * 20 - 10;
     float z = rand() / (float)RAND_MAX * 20 - 10;
     printf("red %f %f %f\n", x, y, z);

     particle_sim_add_particle(
         sim, particle_create(vec3f_init(x, y, z), 1, 1, -0.0005));
     counter0 = 0;
   }

   if (counter1 > 80)
   {
     float x = rand() / (float)RAND_MAX * 20 - 10;
     float y = rand() / (float)RAND_MAX * 20 - 10;
     float z = rand() / (float)RAND_MAX * 20 - 10;
     printf("blue %f %f %f\n", x, y, z);
     particle_sim_add_particle(
         sim, particle_create(vec3f_init(x, y, z), 1, 1, 0.0005));
     counter1 = 0;
   }

   counter0++;
   counter1++;
   */

  // particle_sim_constrain_in_box(sim, vec3f_init(-12, -12, -12),
  //                               vec3f_init(24, 24, 24));
  // particle_sim_set_accelerations(sim, vec3f_init(0, 0, 0));
  // particle_sim_apply_resistance(sim, 0.001);
  // particle_sim_constrain_in_sphere(sim, vec3f_init(0, 0, 0), 40);
  // article_sim_apply_charge_interactions(sim);
  // particle_sim_apply_central_force(sim, vec3f_init(0, 0, 0), 1);

  particle_sim_move(sim, t);

  for (int i = 0; i < sim->particle_cnt; ++i)
  {
    sim->particles[i]->verlet->acc = vec3f_init(0, 0, 0);
    particle_apply_resistance(sim->particles[i], 0.001);
    verlet3f_constrain_in_sphere(sim->particles[i]->verlet, vec3f_init(0, 0, 0),
                                 40);
    particle_apply_central_force(sim->particles[i], vec3f_init(0, 0, 0), 1);
    for (int j = 0; j < i; ++j)
    {
      particle_resolve_collision(sim->particles[i], sim->particles[j]);
      particle_charge_interact(sim->particles[i], sim->particles[j]);
    }
  }

  /*
  for (int i = 0; i < sim->particle_cnt; ++i)
  {
    float x = rand() / (float)RAND_MAX - 0.5;
    float y = rand() / (float)RAND_MAX - 0.5;
    float z = rand() / (float)RAND_MAX - 0.5;
    particle_apply_force(sim->particles[i], vec3f_mul(vec3f_init(x, y, z), 40));
  }
  */


  float fov = 90.0f / 180.0f * M_PI;
  float ar = (float)window_w / (float)window_h;

  camera_on_render(cam);

  mat4f_t P = mat4f_perspective(fov, 1, 500, ar);
  mat4f_t V = camera_get_matrix(cam);

  mat4f_t VP = mat4f_mul_mat4f(P, V);


  float camera_transform_data[16];
  mat4f_to_buffer(VP, camera_transform_data);
  glUniformMatrix4fv(gCameraTransform, 1, GL_FALSE, camera_transform_data);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), 0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float),
                        (void *)(3 * sizeof(float)));
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float),
                        (void *)(6 * sizeof(float)));

  for (int i = 0; i < sim->particle_cnt; ++i)
  {
    verlet3f_t *verlet = sim->particles[i]->verlet;
    if (sim->particles[i]->charge < 0)
    {
      drawObjectScaled(blue_ball_pos, blue_ball_size, verlet->pos,
                       verlet->radius);
    }
    else
    {
      drawObjectScaled(red_ball_pos, red_ball_size, verlet->pos,
                       verlet->radius);
    }
  }

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(2);

  glutPostRedisplay();

  glutSwapBuffers();

  return;
}

static void create_vertex_buffer()
{
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CW);
  glCullFace(GL_BACK);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  mesh3f_t *mesh0 = mesh3f_load_stl_bin("models/sphere-bin.stl");


  object_buffer_t *obj_buffer = object_buffer_create();
  object_buffer_add_mesh_color(obj_buffer, mesh0, vec3f_init(1, 0, 0),
                               &red_ball_pos, &red_ball_size);
  object_buffer_add_mesh_color(obj_buffer, mesh0, vec3f_init(1, 1, 0),
                               &green_ball_pos, &green_ball_size);
  object_buffer_add_mesh_color(obj_buffer, mesh0, vec3f_init(0, 0, 1),
                               &blue_ball_pos, &blue_ball_size);


  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, obj_buffer->size * sizeof(vec3f_t),
               obj_buffer->data, GL_STATIC_DRAW);

  mesh3f_delete(&mesh0);
  object_buffer_delete(&obj_buffer);
}

void add_shader(GLuint shader_program, const char *shader_string,
                GLenum shader_type)
{
  GLuint shader_object = glCreateShader(shader_type);
  if (shader_object == 0)
  {
    fprintf(stderr, "Can not create shader object\n");
    exit(1);
  }

  const GLchar *p[1];
  p[0] = shader_string;
  GLint lengths[1];
  lengths[0] = strlen(shader_string);

  glShaderSource(shader_object, 1, p, lengths);
  glCompileShader(shader_object);

  GLint success;
  glGetShaderiv(shader_object, GL_COMPILE_STATUS, &success);

  if (!success)
  {
    GLchar info_log[1024];
    glGetShaderInfoLog(shader_object, 1024, NULL, info_log);
    fprintf(stderr, "GL Error: %s\n", info_log);
    exit(1);
  }

  glAttachShader(shader_program, shader_object);
}


static void compile_shaders()
{
  GLuint shader_program = glCreateProgram();
  if (shader_program == 0)
  {
    fprintf(stderr, "Can not create shader program\n");
    exit(1);
  }

  char *vs_string = get_file_text_allocated(VS_FILENAME);
  char *fs_string = get_file_text_allocated(FS_FILENAME);

  add_shader(shader_program, vs_string, GL_VERTEX_SHADER);
  add_shader(shader_program, fs_string, GL_FRAGMENT_SHADER);

  free(vs_string);
  free(fs_string);

  GLint success = 0;
  GLchar error_log[1024] = {'\0'};
  glLinkProgram(shader_program);

  glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
  if (success == 0)
  {
    glGetProgramInfoLog(shader_program, 1024, NULL, error_log);
    fprintf(stderr, "GL Error: %s\n", error_log);
    exit(1);
  }

  gObjectTransform = glGetUniformLocation(shader_program, "gObjectTransform");
  if (gObjectTransform == -1)
  {
    fprintf(stderr, "Can not find uniform\n");
    exit(1);
  }

  gCameraTransform = glGetUniformLocation(shader_program, "gCameraTransform");
  if (gCameraTransform == -1)
  {
    fprintf(stderr, "Can not find uniform\n");
    exit(1);
  }

  glValidateProgram(shader_program);
  glGetProgramiv(shader_program, GL_LINK_STATUS, &success);

  if (success == 0)
  {
    glGetProgramInfoLog(shader_program, 1024, NULL, error_log);
    fprintf(stderr, "GL Error: %s\n", error_log);
    exit(1);
  }

  glUseProgram(shader_program);
}

void key_press(unsigned char key, int x, int y)
{
  camera_on_keyboard(cam, key);

  switch (key)
  {
  case 'm':
    t *= 1.1;
    printf("t = %f\n", t);
    break;
  case 'n':
    t /= 1.1;
    printf("t = %f\n", t);
    break;
  }
}

void special_key_press(int key, int x, int y)
{
  camera_on_keyboard(cam, key);
}

void mouse_passive_motion(int x, int y)
{
  camera_on_mouse(cam, x, y);
}

void clean(void)
{
  printf("cleaning\n");
  particle_sim_delete(&sim);
  camera_delete(&cam);
}

void resize(int w, int h)
{
  printf("%i %i\n", w, h);
  window_w = w;
  window_h = h;
  cam->window_w = w;
  cam->window_h = h;
  camera_on_render(cam);
}

int main(int argc, char **argv)
{
  cam = camera_create(vec3f_init(-10, 0, 0), vec3f_init(0, 0, 1),
                      vec3f_init(0, 1, 0), window_w, window_h);

  sim = particle_sim_create();
  prepare_sim();
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);


  int x = 100;
  int y = 100;
  glutInitWindowPosition(x, y);
  glutInitWindowSize(window_w, window_h);
  int window = glutCreateWindow("Window");

  GLenum ret = glewInit();
  if (ret != GLEW_OK)
  {
    return 1;
  }

  GLclampf r = 0.0f, g = 0.0f, b = 0.0f, a = 1.0f;
  glClearColor(r, g, b, a);

  compile_shaders();
  create_vertex_buffer();
  glutDisplayFunc(renderScene);
  glutKeyboardFunc(key_press);
  glutSpecialFunc(special_key_press);
  glutPassiveMotionFunc(mouse_passive_motion);
  glutReshapeFunc(resize);
  atexit(clean);

  glutMainLoop();
  return 0;
}
