#ifndef __WORLD_TRANSF__
#define __WORLD_TRANSF__

#include "../geometry/geometry.h"

typedef struct
{
  vec3f_t pos;
  vec3f_t rot;
  float scale;
} world_transf_t;

world_transf_t world_tranf_init(vec3f_t pos, vec3f_t rot, float scale);

mat4f_t world_transf_get_matrix(world_transf_t transf);

#endif
