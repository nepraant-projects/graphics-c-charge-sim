#include "object_buffer.h"
#include <stdio.h>
#include <stdlib.h>


object_buffer_t *object_buffer_create()
{
  object_buffer_t *buffer = (object_buffer_t *)malloc(sizeof(object_buffer_t));
  if (buffer == NULL)
  {
    return NULL;
  }

  buffer->data = NULL;
  buffer->size = 0;

  return buffer;
}

bool object_buffer_delete(object_buffer_t **buffer)
{
  if (buffer == NULL)
  {
    return false;
  }

  if (*buffer == NULL)
  {
    return true;
  }

  if ((*buffer)->data != NULL)
  {
    free((*buffer)->data);
  }

  free(*buffer);
  *buffer = NULL;

  return true;
}

bool _object_buffer_add_cap(object_buffer_t *buffer, unsigned cap)
{
  if (buffer == NULL)
  {
    return false;
  }

  if (cap == 0)
  {
    return true;
  }

  vec3f_t *tmp =
      (vec3f_t *)realloc(buffer->data, sizeof(vec3f_t) * (buffer->size + cap));
  if (tmp == NULL)
  {
    return false;
  }

  buffer->data = tmp;

  return true;
}

bool object_buffer_add_mesh_color(object_buffer_t *buffer, mesh3f_t *mesh,
                                  vec3f_t color, unsigned *buffer_position,
                                  unsigned *size)
{
  if (buffer == NULL || mesh == NULL)
  {
    return false;
  }

  unsigned position = buffer->size;
  unsigned size_req = mesh->triangle_cnt * 9;
  if (!_object_buffer_add_cap(buffer, size_req))
  {
    return false;
  }

  for (unsigned i = 0; i < mesh->triangle_cnt; ++i)
  {
    for (unsigned j = 0; j < 3; ++j)
    {
      buffer->data[position + 9 * i + 3 * j] = mesh->triangles[i].vertices[j];
      buffer->data[position + 9 * i + 3 * j + 1] = color;
      buffer->data[position + 9 * i + 3 * j + 2] = mesh->normals[i];
      vec3f_print(mesh->normals[i]);
    }
  }

  buffer->size += size_req;

  if (buffer_position != NULL)
  {
    *buffer_position = position;
  }

  if (size != NULL)
  {
    *size = size_req;
  }

  return true;
}

bool object_buffer_add_mesh_colors(object_buffer_t *buffer, mesh3f_t *mesh,
                                   vec3f_t *colors,
                                   unsigned int *buffer_position,
                                   unsigned int *size)
{
  if (buffer == NULL || mesh == NULL)
  {
    return false;
  }

  unsigned position = buffer->size;
  unsigned size_req = mesh->triangle_cnt * 6;
  if (!_object_buffer_add_cap(buffer, size_req))
  {
    return false;
  }

  for (unsigned i = 0; i < mesh->triangle_cnt; ++i)
  {
    for (unsigned j = 0; j < 3; ++j)
    {
      buffer->data[position + 6 * i + 2 * j] = mesh->triangles[i].vertices[j];
      buffer->data[position + 6 * i + 2 * j + 1] = colors[i];
    }
  }

  buffer->size += size_req;

  if (buffer_position != NULL)
  {
    *buffer_position = position;
  }

  if (size != NULL)
  {
    *size = size_req;
  }

  return true;
}
