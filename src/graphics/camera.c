#include "camera.h"
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define CAMERA_DEFAULT_SPEED 0.5

camera_t *camera_create(vec3f_t pos, vec3f_t target, vec3f_t up, int window_w,
                        int window_h)
{
  camera_t *cam = (camera_t *)malloc(sizeof(camera_t));
  if (cam == NULL)
  {
    return NULL;
  }

  cam->pos = pos;
  cam->target = target;
  cam->up = up;
  cam->speed = CAMERA_DEFAULT_SPEED;
  cam->window_w = window_w;
  cam->window_h = window_h;

  vec3f_t h_target = vec3f_normalise(vec3f_init(target.x, 0, target.z));
  float angle = asinf(fabsf(h_target.z));
  if (h_target.z >= 0.0f)
  {
    if (h_target.x >= 0.0f)
    {
      cam->angle_h = 2 * M_PI - angle;
    }
    else
    {
      cam->angle_h = 0.5 * M_PI + angle;
    }
  }
  else
  {
    if (h_target.x >= 0.0f)
    {
      cam->angle_h = angle;
    }
    else
    {
      cam->angle_h = 0.5 * M_PI - angle;
    }
  }

  cam->angle_v = -asin(target.y);
  cam->mouse_x = 0.5 * window_w;
  cam->mouse_y = 0.5 * window_h;

  cam->on_left_edge = false;
  cam->on_lower_edge = false;
  cam->on_right_edge = false;
  cam->on_upper_edge = false;

  return cam;
}

bool camera_delete(camera_t **cam)
{
  if (cam == NULL)
  {
    return false;
  }

  if (*cam == NULL)
  {
    return true;
  }

  free(*cam);
  *cam = NULL;

  return true;
}

void camera_on_keyboard(camera_t *cam, unsigned char key)
{
  if (cam == NULL)
  {
    return;
  }

  vec3f_t left;
  vec3f_t right;
  switch (key)
  {
  case GLUT_KEY_UP:
    cam->pos = vec3f_add(cam->pos, vec3f_mul(cam->target, cam->speed));
    break;
  case GLUT_KEY_DOWN:
    cam->pos = vec3f_add(cam->pos, vec3f_mul(cam->target, -cam->speed));
    break;
  case GLUT_KEY_LEFT:
    left = vec3f_normalise(vec3f_cross(cam->target, cam->up));
    cam->pos = vec3f_add(cam->pos, vec3f_mul(left, cam->speed));
    break;
  case GLUT_KEY_RIGHT:
    right = vec3f_normalise(vec3f_cross(cam->up, cam->target));
    cam->pos = vec3f_add(cam->pos, vec3f_mul(right, cam->speed));
    break;
  case GLUT_KEY_PAGE_UP:
    cam->pos.y += cam->speed;
    break;
  case GLUT_KEY_PAGE_DOWN:
    cam->pos.y -= cam->speed;
    break;
  case '+':
    cam->speed += 0.1;
    break;
  case '-':
    cam->speed -= 0.1;
  }
}


mat4f_t camera_get_matrix(camera_t *cam)
{
  if (cam == NULL)
  {
    return mat4f_zeros();
  }

  vec3f_t N = vec3f_normalise(cam->target);
  vec3f_t U = vec3f_normalise(vec3f_cross(cam->up, N));
  vec3f_t V = vec3f_cross(N, U);

  mat4f_t cam_rot_tranf = mat4f_init(U.x, U.y, U.z, 0, V.x, V.y, V.z, 0, N.x,
                                     N.y, N.z, 0, 0, 0, 0, 1.0f);

  mat4f_t cam_trans_transf =
      mat4f_translate_xyz(-cam->pos.x, -cam->pos.y, -cam->pos.z);

  return mat4f_mul_mat4f(cam_rot_tranf, cam_trans_transf);
}


void _camera_update(camera_t *cam)
{
  vec3f_t y_axis = vec3f_init(0, 1, 0);
  vec3f_t view =
      vec3f_normalise(vec3f_rotate(vec3f_init(1, 0, 0), cam->angle_h, y_axis));
  vec3f_t U = vec3f_normalise(vec3f_cross(y_axis, view));
  view = vec3f_normalise(vec3f_rotate(view, cam->angle_v, U));
  cam->target = view;
  cam->up = vec3f_normalise(vec3f_cross(view, U));
}

void camera_on_mouse(camera_t *cam, int x, int y)
{
  if (cam == NULL)
  {
    return;
  }

  int margin = 10;
  int dx = x - cam->mouse_x;
  int dy = y - cam->mouse_y;
  cam->mouse_x = x;
  cam->mouse_y = y;

  cam->angle_h += (float)dx / 200.0f;
  cam->angle_v += (float)dy / 500.0f;

  if (dx == 0)
  {
    if (x <= margin)
    {
      cam->on_left_edge = true;
    }
    else if (x >= cam->window_w - margin)
    {
      cam->on_right_edge = true;
    }
    else
    {
      cam->on_left_edge = false;
      cam->on_right_edge = false;
    }
  }

  if (dy == 0)
  {
    if (y <= margin)
    {
      cam->on_upper_edge = true;
    }
    else if (y >= cam->window_h - margin)
    {
      cam->on_lower_edge = true;
    }
    else
    {
      cam->on_upper_edge = false;
      cam->on_lower_edge = false;
    }
  }

  _camera_update(cam);
}


void camera_on_render(camera_t *cam)
{
  float edge_step = 0.01;

  if (cam == NULL)
  {
    return;
  }
  bool update = false;

  if (cam->on_left_edge)
  {
    cam->angle_h -= edge_step;
    update = true;
  }

  if (cam->on_right_edge)
  {
    cam->angle_h += edge_step;
    update = true;
  }

  if (cam->on_upper_edge && cam->angle_v > -M_PI / 2)
  {
    cam->angle_v -= edge_step;
    update = true;
  }

  if (cam->on_lower_edge && cam->angle_v < M_PI / 2)
  {
    cam->angle_v += edge_step;
    update = true;
  }

  if (update)
  {
    _camera_update(cam);
  }
}
