#ifndef __PARTICLE_SIM__
#define __PARTICLE_SIM__

#include "particle.h"

typedef struct
{
  particle_t **particles;
  unsigned particle_cap;
  unsigned particle_cnt;
} particle_sim_t;

particle_sim_t *particle_sim_create();

bool particle_sim_delete(particle_sim_t **sim);

bool particle_sim_add_particle(particle_sim_t *sim, particle_t *particle);

bool particle_sim_move(particle_sim_t *sim, float dt);

bool particle_sim_apply_resistance(particle_sim_t *sim, float amount);

bool particle_sim_resolve_collisions(particle_sim_t *sim);

bool particle_sim_apply_force_to_all(particle_sim_t *sim, vec3f_t force);

bool particle_sim_apply_charge_interactions(particle_sim_t *sim);

bool particle_sim_apply_gravity_interactions(particle_sim_t *sim);

bool particle_sim_constrain_in_box(particle_sim_t *sim, vec3f_t box_pos,
                                   vec3f_t box_size);

bool particle_sim_constrain_in_sphere(particle_sim_t *sim, vec3f_t sphere_pos,
                                      float sphere_rad);

bool particle_sim_set_accelerations(particle_sim_t *sim, vec3f_t acceleration);

bool particle_sim_apply_central_force(particle_sim_t *sim, vec3f_t center,
                                      float size);

bool partilce_sim_apply_magnetic_field(particle_sim_t *sim, vec3f_t B);

#endif
