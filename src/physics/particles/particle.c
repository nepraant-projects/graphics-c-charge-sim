#include "particle.h"
#include <math.h>
#include <stdlib.h>

#define COULOMB_CONSTANT 8987551793.0732
#define GRAVITATIONAL_CONSTANT 6.674E-11

particle_t *particle_create(vec3f_t pos, float radius, float weight,
                            float charge)
{
  particle_t *particle = (particle_t *)malloc(sizeof(particle_t));
  if (particle == NULL)
  {
    return NULL;
  }

  particle->verlet = verlet3f_create(pos, weight, radius);
  if (particle->verlet == NULL)
  {
    free(particle);
    return NULL;
  }

  particle->charge = charge;

  return particle;
}

bool particle_delete(particle_t **particle)
{
  if (particle == NULL)
  {
    return false;
  }

  if (*particle == NULL)
  {
    return true;
  }

  verlet3f_delete(&(*particle)->verlet);
  free(*particle);
  *particle = NULL;

  return true;
}

bool particle_move(particle_t *particle, float dt)
{
  if (particle == NULL)
  {
    return false;
  }

  verlet3f_move(particle->verlet, dt);

  return true;
}

bool particle_apply_resistance(particle_t *particle, float amount)
{
  if (particle == NULL)
  {
    return false;
  }

  verlet3f_apply_resistance(particle->verlet, amount);

  return true;
}

bool particle_resolve_collision(particle_t *particle0, particle_t *particle1)
{
  if (particle0 == NULL || particle1 == NULL)
  {
    return false;
  }

  verlet3f_resolve_collision(particle0->verlet, particle1->verlet);

  return true;
}

bool particle_apply_force(particle_t *particle, vec3f_t force)
{
  if (particle == NULL)
  {
    return false;
  }

  verlet3f_apply_force(particle->verlet, force);

  return true;
}

bool particle_charge_interact(particle_t *particle0, particle_t *particle1)
{
  if (particle0 == NULL || particle1 == NULL)
  {
    return false;
  }

  vec3f_t sub = vec3f_sub(particle0->verlet->pos, particle1->verlet->pos);
  float sub_size = vec3f_size(sub);

  vec3f_t F0 = vec3f_mul(sub, COULOMB_CONSTANT * particle0->charge *
                                  particle1->charge / powf(sub_size, 3));

  vec3f_t F1 = vec3f_mul(F0, -1);

  particle_apply_force(particle0, F0);
  particle_apply_force(particle1, F1);

  return true;
}

bool particle_gravity_interact(particle_t *particle0, particle_t *particle1)
{
  if (particle0 == NULL || particle1 == NULL)
  {
    return false;
  }

  vec3f_t sub = vec3f_sub(particle0->verlet->pos, particle1->verlet->pos);
  float sub_size = vec3f_size(sub);

  vec3f_t F0 =
      vec3f_mul(sub, -GRAVITATIONAL_CONSTANT * particle0->verlet->weight *
                         particle1->verlet->weight / powf(sub_size, 3));

  vec3f_t F1 = vec3f_mul(F0, -1);

  particle_apply_force(particle0, F0);
  particle_apply_force(particle1, F1);

  return true;
}

bool particle_apply_central_force(particle_t *particle, vec3f_t center,
                                  float size)
{
  if (particle == NULL)
  {
    return false;
  }

  vec3f_t force_dir = vec3f_sub(center, particle->verlet->pos);
  vec3f_t force = vec3f_mul(force_dir, size / vec3f_size(force_dir));

  particle_apply_force(particle, force);

  return true;
}

bool particle_apply_magnetic_field(particle_t *particle, vec3f_t B)
{
  if (particle == NULL)
  {
    return false;
  }

  vec3f_t vel = vec3f_sub(particle->verlet->pos, particle->verlet->prev_pos);
  vec3f_t force = vec3f_mul(vec3f_cross(vel, B), particle->charge);

  particle_apply_force(particle, force);

  return true;
}
