#include "particle_sim.h"
#include "particle.h"
#include <math.h>
#include <stdlib.h>

#define PARTICLE_SIM_DEFAULT_PARTICLE_CAP 16

particle_sim_t *particle_sim_create()
{
  particle_sim_t *sim = (particle_sim_t *)malloc(sizeof(particle_sim_t));
  if (sim == NULL)
  {
    return NULL;
  }

  sim->particles = (particle_t **)malloc(sizeof(particle_t *) *
                                         PARTICLE_SIM_DEFAULT_PARTICLE_CAP);
  if (sim->particles == NULL)
  {
    free(sim);
    return NULL;
  }

  sim->particle_cap = PARTICLE_SIM_DEFAULT_PARTICLE_CAP;
  sim->particle_cnt = 0;

  return sim;
}

bool particle_sim_delete(particle_sim_t **sim)
{
  if (sim == NULL)
  {
    return false;
  }

  if (*sim == NULL)
  {
    return true;
  }

  for (unsigned i = 0; i < (*sim)->particle_cnt; ++i)
  {
    particle_delete(&(*sim)->particles[i]);
  }

  free((*sim)->particles);
  free(*sim);
  *sim = NULL;

  return true;
}

bool _particle_sim_add_cap(particle_sim_t *sim)
{
  if (sim == NULL)
  {
    return false;
  }

  particle_t **tmp = (particle_t **)realloc(
      sim->particles,
      sizeof(particle_t *) *
          (sim->particle_cap + PARTICLE_SIM_DEFAULT_PARTICLE_CAP));

  if (tmp == NULL)
  {
    return false;
  }

  sim->particle_cap += PARTICLE_SIM_DEFAULT_PARTICLE_CAP;
  sim->particles = tmp;

  return true;
}

bool particle_sim_add_particle(particle_sim_t *sim, particle_t *particle)
{
  if (sim == NULL || particle == NULL)
  {
    return false;
  }

  if (sim->particle_cnt == sim->particle_cap && !_particle_sim_add_cap(sim))
  {
    return false;
  }

  sim->particles[sim->particle_cnt] = particle;

  ++sim->particle_cnt;

  return true;
}

bool particle_sim_move(particle_sim_t *sim, float dt)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    particle_move(sim->particles[i], dt);
  }

  return true;
}

bool particle_sim_apply_resistance(particle_sim_t *sim, float amount)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    particle_apply_resistance(sim->particles[i], amount);
  }

  return true;
}

bool particle_sim_resolve_collisions(particle_sim_t *sim)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    for (unsigned j = 0; j < i; ++j)
    {
      particle_resolve_collision(sim->particles[i], sim->particles[j]);
    }
  }

  return true;
}

bool particle_sim_apply_force_to_all(particle_sim_t *sim, vec3f_t force)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    particle_apply_force(sim->particles[i], force);
  }

  return true;
}

bool particle_sim_apply_charge_interactions(particle_sim_t *sim)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    for (unsigned j = 0; j < i; ++j)
    {
      particle_charge_interact(sim->particles[i], sim->particles[j]);
    }
  }

  return true;
}

bool particle_sim_apply_gravity_interactions(particle_sim_t *sim)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    for (unsigned j = 0; j < i; ++j)
    {
      particle_gravity_interact(sim->particles[i], sim->particles[j]);
    }
  }

  return true;
}

bool particle_sim_constrain_in_box(particle_sim_t *sim, vec3f_t box_pos,
                                   vec3f_t box_size)
{
  if (sim == NULL)
  {
    return false;
  }

  float x_min = fminf(box_pos.x, box_pos.x + box_size.x);
  float x_max = fmaxf(box_pos.x, box_pos.x + box_size.x);
  float y_min = fminf(box_pos.y, box_pos.y + box_size.y);
  float y_max = fmaxf(box_pos.y, box_pos.y + box_size.y);
  float z_min = fminf(box_pos.z, box_pos.z + box_size.z);
  float z_max = fmaxf(box_pos.z, box_pos.z + box_size.z);


  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    verlet3f_constrain_x_min(sim->particles[i]->verlet, x_min);
    verlet3f_constrain_x_max(sim->particles[i]->verlet, x_max);
    verlet3f_constrain_y_min(sim->particles[i]->verlet, y_min);
    verlet3f_constrain_y_max(sim->particles[i]->verlet, y_max);
    verlet3f_constrain_z_min(sim->particles[i]->verlet, z_min);
    verlet3f_constrain_z_max(sim->particles[i]->verlet, z_max);
  }

  return true;
}

bool particle_sim_constrain_in_sphere(particle_sim_t *sim, vec3f_t sphere_pos,
                                      float sphere_rad)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    verlet3f_constrain_in_sphere(sim->particles[i]->verlet, sphere_pos,
                                 sphere_rad);
  }

  return true;
}


bool particle_sim_set_accelerations(particle_sim_t *sim, vec3f_t acceleration)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    sim->particles[i]->verlet->acc = acceleration;
  }

  return true;
}


bool particle_sim_apply_central_force(particle_sim_t *sim, vec3f_t center,
                                      float size)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    particle_apply_central_force(sim->particles[i], center, size);
  }

  return true;
}

bool partilce_sim_apply_magnetic_field(particle_sim_t *sim, vec3f_t B)
{
  if (sim == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < sim->particle_cnt; ++i)
  {
    particle_apply_magnetic_field(sim->particles[i], B);
  }

  return true;
}
