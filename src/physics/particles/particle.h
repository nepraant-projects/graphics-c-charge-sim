#ifndef __PARTICLE__
#define __PARTICLE__

#include "../verlets/verlet3f.h"

typedef struct
{
  verlet3f_t *verlet;
  float charge;
} particle_t;

particle_t *particle_create(vec3f_t pos, float radius, float weight,
                            float charge);

bool particle_delete(particle_t **particle);

bool particle_move(particle_t *particle, float dt);

bool particle_apply_resistance(particle_t *particle, float amount);

bool particle_resolve_collision(particle_t *particle0, particle_t *particle1);

bool particle_apply_force(particle_t *particle, vec3f_t force);

bool particle_charge_interact(particle_t *particle0, particle_t *particle1);

bool particle_gravity_interact(particle_t *particle0, particle_t *particle1);

bool particle_apply_central_force(particle_t *particle, vec3f_t center,
                                  float size);

bool particle_apply_magnetic_field(particle_t *particle, vec3f_t B);

#endif
