#ifndef __VERLET3F__
#define __VERLET3F__

#include "../../geometry/3f/geometry3f.h"
#include <stdbool.h>

typedef struct
{
  vec3f_t pos;
  vec3f_t prev_pos;
  vec3f_t acc;

  float weight;
  float radius;
} verlet3f_t;

verlet3f_t *verlet3f_create(vec3f_t pos, float weight, float radius);

bool verlet3f_delete(verlet3f_t **verlet);

bool verlet3f_move(verlet3f_t *verlet, float dt);

bool verlet3f_resolve_collision(verlet3f_t *verlet0, verlet3f_t *verlet1);

bool verlet3f_constrain_x_max(verlet3f_t *verlet, float x);

bool verlet3f_constrain_x_min(verlet3f_t *verlet, float x);

bool verlet3f_constrain_y_max(verlet3f_t *verlet, float y);

bool verlet3f_constrain_y_min(verlet3f_t *verlet, float y);

bool verlet3f_constrain_z_max(verlet3f_t *verlet, float z);

bool verlet3f_constrain_z_min(verlet3f_t *verlet, float z);

bool verlet3f_constrain_in_sphere(verlet3f_t *verlet, vec3f_t sphere_position,
                                  float sphere_radius);

bool verlet3f_constrain_out_sphere(verlet3f_t *verlet, vec3f_t sphere_position,
                                   float sphere_radius);

bool verlet3f_constrain_distance(verlet3f_t *verlet0, verlet3f_t *verlet1,
                                 float distance);

bool verlet3f_constrain_position(verlet3f_t *verlet, vec3f_t position);

bool verlet3f_apply_force(verlet3f_t *verlet, vec3f_t force);

bool verlet3f_apply_force_in_area(verlet3f_t *verlet, float x_min, float x_max,
                                  float y_min, float y_max, float z_min,
                                  float z_max, vec3f_t force);

bool verlet3f_apply_resistance(verlet3f_t *verlet, float amount);

#endif
