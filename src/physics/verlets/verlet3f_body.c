#include "verlet3f_body.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define VERLET3F_BODY_DEFAULT_VERLET_CAP 8
#define VERLET3F_BODY_DEFAULT_CONSTRAIN_CAP 4

verlet3f_body_t *verlet3f_body_create()
{
  verlet3f_body_t *body = (verlet3f_body_t *)malloc(sizeof(verlet3f_body_t));
  if (body == NULL)
  {
    return NULL;
  }

  body->verlets = (verlet3f_t **)malloc(sizeof(verlet3f_t *) *
                                        VERLET3F_BODY_DEFAULT_VERLET_CAP);
  if (body->verlets == NULL)
  {
    free(body);
    return NULL;
  }

  body->constrains = (verlet3f_body_constrain_t *)malloc(
      sizeof(verlet3f_body_constrain_t) * VERLET3F_BODY_DEFAULT_CONSTRAIN_CAP);
  if (body->constrains == NULL)
  {
    free(body->verlets);
    free(body);
    return NULL;
  }

  body->verlet_cap = VERLET3F_BODY_DEFAULT_VERLET_CAP;
  body->verlet_cnt = 0;
  body->constrain_cap = VERLET3F_BODY_DEFAULT_CONSTRAIN_CAP;
  body->constrain_cnt = 0;

  return body;
}

bool verlet3f_body_delete(verlet3f_body_t **body)
{
  if (body == NULL)
  {
    return false;
  }

  if (*body == NULL)
  {
    return true;
  }

  for (unsigned i = 0; i < (*body)->verlet_cnt; ++i)
  {
    verlet3f_delete(&(*body)->verlets[i]);
  }

  free((*body)->constrains);
  free((*body)->verlets);
  free(*body);
  *body = NULL;

  return true;
}

bool _verlet3f_body_add_verlet_capacity(verlet3f_body_t *body)
{
  if (body == NULL)
  {
    return false;
  }

  verlet3f_t **tmp = (verlet3f_t **)realloc(
      body->verlets, sizeof(verlet3f_t *) *
                         (body->verlet_cap + VERLET3F_BODY_DEFAULT_VERLET_CAP));
  if (tmp == NULL)
  {
    return false;
  }

  body->verlets = tmp;
  body->verlet_cap += VERLET3F_BODY_DEFAULT_VERLET_CAP;

  return true;
}

bool verlet3f_body_add_verlet(verlet3f_body_t *body, verlet3f_t *verlet)
{
  if (body == NULL || verlet == NULL)
  {
    return false;
  }

  if (body->verlet_cnt == body->verlet_cap &&
      !_verlet3f_body_add_verlet_capacity(body))
  {
    return false;
  }

  body->verlets[body->verlet_cnt] = verlet;
  ++body->verlet_cnt;

  return true;
}

bool _verlet3f_body_add_constrain_capacity(verlet3f_body_t *body)
{
  if (body == NULL)
  {
    return false;
  }

  verlet3f_body_constrain_t *tmp = (verlet3f_body_constrain_t *)realloc(
      body->constrains,
      sizeof(verlet3f_body_constrain_t) *
          (body->constrain_cap + VERLET3F_BODY_DEFAULT_CONSTRAIN_CAP));
  if (tmp == NULL)
  {
    return false;
  }

  body->constrains = tmp;
  body->constrain_cap += VERLET3F_BODY_DEFAULT_CONSTRAIN_CAP;

  return true;
}

bool verlet3f_body_add_constrain(verlet3f_body_t *body,
                                 verlet3f_body_constrain_t constrain)
{
  if (body == NULL)
  {
    return false;
  }

  if (body->constrain_cnt == body->constrain_cap &&
      !_verlet3f_body_add_constrain_capacity(body))
  {
    return false;
  }

  body->constrains[body->constrain_cnt] = constrain;
  ++body->constrain_cnt;

  return true;
}

bool verlet3f_body_add_dist_constrain(verlet3f_body_t *body,
                                      unsigned verlet0_index,
                                      unsigned verlet1_index)
{
  if (body == NULL || verlet0_index == verlet1_index ||
      verlet0_index >= body->verlet_cnt || verlet1_index >= body->verlet_cnt)
  {
    return false;
  }

  vec3f_t dist_vec = vec3f_sub(body->verlets[verlet0_index]->pos,
                               body->verlets[verlet1_index]->pos);
  float dist = sqrtf(vec3f_dot(dist_vec, dist_vec));

  verlet3f_body_constrain_t constrain;
  constrain.type = VERLET3F_BODY_CONSTRAIN_DIST;
  constrain.data.dist.dist = dist;
  constrain.data.dist.verlet0 = body->verlets[verlet0_index];
  constrain.data.dist.verlet1 = body->verlets[verlet1_index];

  return verlet3f_body_add_constrain(body, constrain);
}

bool verlet3f_body_add_pos_constrain(verlet3f_body_t *body,
                                     unsigned verlet_index, vec3f_t position)
{
  if (body == NULL || verlet_index >= body->verlet_cnt)
  {
    return false;
  }

  verlet3f_body_constrain_t constrain;
  constrain.type = VERLET3F_BODY_CONSTRAIN_POS;
  constrain.data.pos.verlet = body->verlets[verlet_index];
  constrain.data.pos.pos = position;

  return verlet3f_body_add_constrain(body, constrain);
}

bool verlet3f_body_add_all_dist_constrains(verlet3f_body_t *body)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    for (unsigned j = 0; j < i; ++j)
    {
      if (!verlet3f_body_add_dist_constrain(body, i, j))
      {
        return false;
      }
    }
  }

  return true;
}

bool verlet3f_body_resolve_constrains(verlet3f_body_t *body)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->constrain_cnt; ++i)
  {
    switch (body->constrains[i].type)
    {
    case VERLET3F_BODY_CONSTRAIN_DIST:
      verlet3f_constrain_distance(body->constrains[i].data.dist.verlet0,
                                  body->constrains[i].data.dist.verlet1,
                                  body->constrains[i].data.dist.dist);
      break;
    case VERLET3F_BODY_CONSTRAIN_INNER_COLLISIONS:
      for (unsigned i = 0; i < body->verlet_cnt; ++i)
      {
        for (unsigned j = 0; j < i; ++j)
        {
          verlet3f_resolve_collision(body->verlets[i], body->verlets[j]);
        }
      }
      break;
    case VERLET3F_BODY_CONSTRAIN_POS:
      verlet3f_constrain_position(body->constrains[i].data.pos.verlet,
                                  body->constrains[i].data.pos.pos);
      break;
    default:
      break;
    }
  }

  return true;
}

bool verlet3f_body_move(verlet3f_body_t *body, float dt)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_move(body->verlets[i], dt);
  }

  return true;
}

bool verlet3f_body_set_acceleration(verlet3f_body_t *body, vec3f_t acc)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    body->verlets[i]->acc = acc;
  }

  return true;
}

bool verlet3f_body_resolve_collisions(verlet3f_body_t *body0,
                                      verlet3f_body_t *body1)
{
  if (body0 == NULL || body1 == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body0->verlet_cnt; ++i)
  {
    for (unsigned j = 0; j < body1->verlet_cnt; ++j)
    {
      verlet3f_resolve_collision(body0->verlets[i], body1->verlets[j]);
    }
  }

  return true;
}

bool verlet3f_body_constrain_x_max(verlet3f_body_t *body, float x)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_x_max(body->verlets[i], x);
  }

  return true;
}

bool verlet3f_body_constrain_x_min(verlet3f_body_t *body, float x)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_x_min(body->verlets[i], x);
  }

  return true;
}

bool verlet3f_body_constrain_y_max(verlet3f_body_t *body, float y)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_y_max(body->verlets[i], y);
  }

  return true;
}

bool verlet3f_body_constrain_y_min(verlet3f_body_t *body, float y)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_y_min(body->verlets[i], y);
  }

  return true;
}

bool verlet3f_body_constrain_z_max(verlet3f_body_t *body, float z)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_z_max(body->verlets[i], z);
  }

  return true;
}

bool verlet3f_body_constrain_z_min(verlet3f_body_t *body, float z)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_z_min(body->verlets[i], z);
  }

  return true;
}

bool verlet3f_body_constrain_in_sphere(verlet3f_body_t *body,
                                       vec3f_t sphere_position,
                                       float sphere_radius)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_in_sphere(body->verlets[i], sphere_position,
                                 sphere_radius);
  }

  return true;
}

bool verlet3f_body_constrain_out_sphere(verlet3f_body_t *body,
                                        vec3f_t sphere_position,
                                        float sphere_radius)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_constrain_out_sphere(body->verlets[i], sphere_position,
                                  sphere_radius);
  }

  return true;
}

bool verlet3f_body_apply_force(verlet3f_body_t *body, vec3f_t force)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_apply_force(body->verlets[i], force);
  }

  return true;
}

bool verlet3f_body_apply_force_in_area(verlet3f_body_t *body, float x_min,
                                       float x_max, float y_min, float y_max,
                                       float z_min, float z_max, vec3f_t force)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_apply_force_in_area(body->verlets[i], x_min, x_max, y_min, y_max,
                                 z_min, z_max, force);
  }

  return true;
}


bool verlet3f_body_apply_resistance(verlet3f_body_t *body, float amount)
{
  if (body == NULL)
  {
    return false;
  }

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    verlet3f_apply_resistance(body->verlets[i], amount);
  }

  return true;
}

bool verlet3f_body_bounding_box(verlet3f_body_t *body, vec3f_t *pos,
                                vec3f_t *size)
{
  if (body == NULL)
  {
    return NULL;
  }

  float x_min = INFINITY;
  float x_max = -INFINITY;
  float y_min = INFINITY;
  float y_max = -INFINITY;
  float z_min = INFINITY;
  float z_max = -INFINITY;

  for (unsigned i = 0; i < body->verlet_cnt; ++i)
  {
    float verlet_x_min = body->verlets[i]->pos.x - body->verlets[i]->radius;
    float verlet_x_max = body->verlets[i]->pos.x + body->verlets[i]->radius;
    float verlet_y_min = body->verlets[i]->pos.y - body->verlets[i]->radius;
    float verlet_y_max = body->verlets[i]->pos.y + body->verlets[i]->radius;
    float verlet_z_min = body->verlets[i]->pos.z - body->verlets[i]->radius;
    float verlet_z_max = body->verlets[i]->pos.z + body->verlets[i]->radius;

    if (verlet_x_min < x_min)
    {
      x_min = verlet_x_min;
    }

    if (verlet_x_max > x_max)
    {
      x_max = verlet_x_max;
    }

    if (verlet_y_min < y_min)
    {
      y_min = verlet_y_min;
    }

    if (verlet_y_max > y_max)
    {
      y_max = verlet_y_max;
    }


    if (verlet_z_min < z_min)
    {
      z_min = verlet_z_min;
    }

    if (verlet_z_max > z_max)
    {
      z_max = verlet_z_max;
    }
  }

  if (pos != NULL)
  {
    *pos = vec3f_init(x_min, y_min, z_min);
  }

  if (size != NULL)
  {
    *size = vec3f_init(x_max - x_min, y_max - y_min, z_max - z_min);
  }

  return true;
}


bool verlet3f_body_bounding_boxex_collide(vec3f_t pos0, vec3f_t size0,
                                          vec3f_t pos1, vec3f_t size1)
{
  return pos0.x < pos1.x + size1.x && pos0.x + size0.x > pos1.x &&
         pos0.y < pos1.y + size1.y && size0.y + pos0.y > pos1.y &&
         pos0.z < pos1.z + size1.z && size0.z + pos0.z > pos1.z;
}
