#include "verlet3f.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

verlet3f_t *verlet3f_create(vec3f_t pos, float weight, float radius)
{
  verlet3f_t *verlet = (verlet3f_t *)malloc(sizeof(verlet3f_t));
  if (verlet == NULL)
  {
    return NULL;
  }

  verlet->pos = pos;
  verlet->prev_pos = pos;
  verlet->weight = weight;
  verlet->radius = radius;

  return verlet;
}

bool verlet3f_delete(verlet3f_t **verlet)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (*verlet == NULL)
  {
    return true;
  }

  free(*verlet);
  *verlet = NULL;

  return true;
}

bool verlet3f_move(verlet3f_t *verlet, float dt)
{
  if (verlet == NULL)
  {
    return false;
  }

  vec3f_t vel = vec3f_sub(verlet->pos, verlet->prev_pos);
  verlet->prev_pos = verlet->pos;
  verlet->pos =
      vec3f_add(vec3f_add(verlet->pos, vel), vec3f_mul(verlet->acc, dt * dt));
  verlet->acc = (vec3f_t){0, 0, 0};

  return true;
}

bool verlet3f_resolve_collision(verlet3f_t *verlet0, verlet3f_t *verlet1)
{
  if (verlet0 == NULL || verlet1 == NULL)
  {
    return false;
  }

  vec3f_t dist_vec = vec3f_sub(verlet0->pos, verlet1->pos);
  float dist_sq = vec3f_dot(dist_vec, dist_vec);
  float dist_min = verlet0->radius + verlet1->radius;

  if (dist_sq >= dist_min * dist_min)
  {
    return true;
  }

  float dist = sqrt(dist_sq);
  float change = dist_min - dist;
  float mul = change / dist;
  float m0_sq = verlet0->weight * verlet0->weight;
  float m1_sq = verlet1->weight * verlet1->weight;
  float mul0 = mul * (m1_sq) / (m0_sq + m1_sq);
  float mul1 = -mul * (m0_sq) / (m0_sq + m1_sq);
  verlet0->pos = vec3f_add(verlet0->pos, vec3f_mul(dist_vec, mul0));
  verlet1->pos = vec3f_add(verlet1->pos, vec3f_mul(dist_vec, mul1));

  return true;
}

bool verlet3f_constrain_x_max(verlet3f_t *verlet, float x)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (verlet->pos.x > x)
  {
    verlet->pos.x = x;
  }

  return true;
}

bool verlet3f_constrain_x_min(verlet3f_t *verlet, float x)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (verlet->pos.x < x)
  {
    verlet->pos.x = x;
  }

  return true;
}

bool verlet3f_constrain_y_max(verlet3f_t *verlet, float y)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (verlet->pos.y > y)
  {
    verlet->pos.y = y;
  }

  return true;
}


bool verlet3f_constrain_y_min(verlet3f_t *verlet, float y)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (verlet->pos.y < y)
  {
    verlet->pos.y = y;
  }

  return true;
}

bool verlet3f_constrain_z_max(verlet3f_t *verlet, float z)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (verlet->pos.z > z)
  {
    verlet->pos.z = z;
  }

  return true;
}

bool verlet3f_constrain_z_min(verlet3f_t *verlet, float z)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (verlet->pos.z < z)
  {
    verlet->pos.z = z;
  }

  return true;
}

bool verlet3f_constrain_in_sphere(verlet3f_t *verlet, vec3f_t sphere_position,
                                  float sphere_radius)
{
  if (verlet == NULL)
  {
    return false;
  }

  float max_dist = -verlet->radius + sphere_radius;
  vec3f_t dist_vec = vec3f_sub(verlet->pos, sphere_position);
  float dist_sq = vec3f_dot(dist_vec, dist_vec);
  if (dist_sq < max_dist * max_dist)
  {
    return true;
  }

  float dist = sqrtf(dist_sq);
  float diff = max_dist - dist;
  double mul = diff / dist;
  verlet->pos = vec3f_add(verlet->pos, vec3f_mul(dist_vec, mul));

  return true;
}

bool verlet3f_constrain_out_sphere(verlet3f_t *verlet, vec3f_t sphere_position,
                                   float sphere_radius)
{
  if (verlet == NULL)
  {
    return false;
  }

  float max_dist = verlet->radius + sphere_radius;
  vec3f_t dist_vec = vec3f_sub(verlet->pos, sphere_position);
  float dist_sq = vec3f_dot(dist_vec, dist_vec);
  if (dist_sq > max_dist * max_dist)
  {
    return true;
  }

  float dist = sqrtf(dist_sq);
  float diff = max_dist - dist;
  double mul = diff / dist;
  verlet->pos = vec3f_add(verlet->pos, vec3f_mul(dist_vec, -mul));

  return true;
}

bool verlet3f_constrain_distance(verlet3f_t *verlet0, verlet3f_t *verlet1,
                                 float distance)
{
  if (verlet0 == NULL || verlet1 == NULL)
  {
    return false;
  }

  vec3f_t dist_vec = vec3f_sub(verlet0->pos, verlet1->pos);
  float real_distance = sqrtf(vec3f_dot(dist_vec, dist_vec));
  float weight0_sq = verlet0->weight * verlet0->weight;
  float weight1_sq = verlet1->weight * verlet1->weight;

  float mul = (distance - real_distance) / (2 * real_distance);
  float mul0 = mul * verlet0->weight / (verlet0->weight + verlet1->weight);
  float mul1 = -mul * verlet1->weight / (verlet0->weight + verlet1->weight);
  verlet0->pos = vec3f_add(verlet0->pos, vec3f_mul(dist_vec, mul0));
  verlet1->pos = vec3f_add(verlet1->pos, vec3f_mul(dist_vec, mul1));

  return true;
}

bool verlet3f_constrain_position(verlet3f_t *verlet, vec3f_t position)
{
  if (verlet == NULL)
  {
    return false;
  }

  verlet->pos = position;
  verlet->prev_pos = position;

  return true;
}

bool verlet3f_apply_force(verlet3f_t *verlet, vec3f_t force)
{
  if (verlet == NULL)
  {
    return false;
  }

  vec3f_t acc = vec3f_mul(force, 1.0f / verlet->weight);
  verlet->acc = vec3f_add(verlet->acc, acc);

  return true;
}

bool verlet3f_apply_force_in_area(verlet3f_t *verlet, float x_min, float x_max,
                                  float y_min, float y_max, float z_min,
                                  float z_max, vec3f_t force)
{
  if (verlet == NULL)
  {
    return false;
  }

  if (verlet->pos.x < x_min || verlet->pos.x > x_max)
  {
    return true;
  }

  if (verlet->pos.y < y_min || verlet->pos.y > y_max)
  {
    return true;
  }

  if (verlet->pos.z < z_min || verlet->pos.z > z_max)
  {
    return true;
  }

  verlet3f_apply_force(verlet, force);

  return true;
}

bool verlet3f_apply_resistance(verlet3f_t *verlet, float amount)
{
  if (verlet == NULL)
  {
    return false;
  }

  vec3f_t velocity = vec3f_sub(verlet->pos, verlet->prev_pos);
  verlet->prev_pos = vec3f_sub(verlet->pos, vec3f_mul(velocity, 1 - amount));

  return true;
}
