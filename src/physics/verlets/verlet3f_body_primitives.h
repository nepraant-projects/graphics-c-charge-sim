#ifndef __VERLET3F_BODY_PRIMITIVES__
#define __VERLET3F_BODY_PRIMITIVES__

#include "verlet3f_body.h"


verlet3f_body_t *verlet3f_body_primitive_cuboid(float verlet_radius,
                                                float verlet_weight,
                                                vec3f_t position, float w,
                                                float h, float d);


verlet3f_body_t *verlet3f_body_primitive_chain(float verlet_radius,
                                               float verlet_weight, vec3f_t v0,
                                               vec3f_t v1, unsigned n);


verlet3f_body_t *verlet3f_body_primitive_tetrahedron(float verlet_radius,
                                                     float verlet_weight,
                                                     vec3f_t position,
                                                     float size);


verlet3f_body_t *verlet3f_body_primitive_pyramid(float verlet_radius,
                                                 float verlet_weight,
                                                 vec3f_t position, float size,
                                                 float heigth);

verlet3f_body_t *verlet3f_body_primitive_sphere(float verlet_radius,
                                                float verlet_weight,
                                                vec3f_t position, float radius,
                                                unsigned v_divs,
                                                unsigned r_divs);

verlet3f_body_t *verlet3f_body_primitive_fixed_chain(float verlet_radius,
                                                     float verlet_weight,
                                                     vec3f_t v0, vec3f_t v1,
                                                     unsigned n, bool fixed0,
                                                     bool fixed1);

verlet3f_body_t *verlet3f_body_primitive_net(float verlet_radius,
                                             float verlet_weight, vec3f_t pos,
                                             vec3f_t size0, vec3f_t size1,
                                             unsigned n0, unsigned n1);


verlet3f_body_t *
verlet3f_body_primitive_fixed_net(float verlet_radius, float verlet_weight,
                                  vec3f_t pos, vec3f_t size0, vec3f_t size1,
                                  unsigned n0, unsigned n1, bool fixed00,
                                  bool fixed01, bool fixed10, bool fixed11);


verlet3f_body_t *verlet3f_body_primitive_fixed_sides_net(
    float verlet_radius, float verlet_weight, vec3f_t pos, vec3f_t size0,
    vec3f_t size1, unsigned n0, unsigned n1);

#endif
