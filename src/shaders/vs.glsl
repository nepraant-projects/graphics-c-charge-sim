#version 330 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 ColorIn;
layout (location = 2) in vec3 NormIn;

uniform mat4 gObjectTransform;
uniform mat4 gCameraTransform;

out vec3 Color;
out vec3 Norm;

void main() 
{
  gl_Position = gCameraTransform * gObjectTransform * vec4(Position, 1.0);
  Color = ColorIn;
  Norm = NormIn;
}
