#version 330 core
in vec3 Color;
in vec3 Norm;

out vec4 FragColor;

void main()
{
  vec3 white = vec3(1, 1, 1);
  vec3 axis = vec3(0, 1, 0);

  float d_sq = dot(Norm, axis);
  float d;
  if (d_sq < 0)
  {
    d = -sqrt(abs(d_sq));
  } 
  else 
  {
    d = sqrt(d_sq);
  }

  vec3 updated_color = Color * (d + 1)/ 2;
  

  FragColor = vec4(updated_color, 1);
}
