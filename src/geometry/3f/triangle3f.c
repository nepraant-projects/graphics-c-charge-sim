#include "triangle3f.h"
#include <stdio.h>

triangle3f_t triangle3f_init(vec3f_t v0, vec3f_t v1, vec3f_t v2)
{
  return (triangle3f_t){.vertices = {v0, v1, v2}};
}

void triangle3f_print(triangle3f_t t)
{
  printf("triangle:\n");
  for (int i = 0; i < 3; ++i)
  {
    putchar('\t');
    vec3f_print(t.vertices[i]);
  }
}
