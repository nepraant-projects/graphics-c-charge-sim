#ifndef __MAT4F__
#define __MAT4F__

#include "vec4f.h"

typedef struct
{
  float data[4][4];
} mat4f_t;

mat4f_t mat4f_init(float x00, float x01, float x02, float x03, float x10,
                   float x11, float x12, float x13, float x20, float x21,
                   float x22, float x23, float x30, float x31, float x32,
                   float x33);

mat4f_t mat4f_add(mat4f_t m0, mat4f_t m1);

mat4f_t mat4f_sub(mat4f_t m0, mat4f_t m1);

mat4f_t mat4f_mul_scal(mat4f_t m, float s);

mat4f_t mat4f_mul_mat4f(mat4f_t m0, mat4f_t m1);

vec4f_t mat4f_mul_vec4f(mat4f_t m, vec4f_t v);

mat4f_t mat4f_eye();

mat4f_t mat4f_zeros();

mat4f_t mat4f_rot_x(float angle_rad);

mat4f_t mat4f_rot_y(float angle_rad);

mat4f_t mat4f_rot_z(float angle_rad);

mat4f_t mat4f_rot_xyz(float angle_rad_x, float angle_rad_y, float angle_rad_z);

mat4f_t mat4f_translate_x(float x);

mat4f_t mat4f_translate_y(float y);

mat4f_t mat4f_translate_z(float z);

mat4f_t mat4f_translate_xyz(float x, float y, float z);

mat4f_t mat4f_scale(float s);

mat4f_t mat4f_perspective(float fov, float near, float far,
                          float aspect_ration);

void mat4f_print(mat4f_t m);

#endif
