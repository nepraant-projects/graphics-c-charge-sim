CC = gcc
LFLAGS = -lm $(shell pkg-config --libs glew glut gl)
CFLAGS = -Wall -g -O2 -fsanitize=address
NUM_THR = 8

MAINS = ./src/demo0.c 
TARGET = ./src/demo0.c
BIN = ./build/bin/demo0

BUILD_DIR = ./build
C_FILES = $(filter-out $(MAINS),$(shell find . -type f -name '*.c'))
O_FILES = $(patsubst ./%,$(BUILD_DIR)/obj/%,$(patsubst %.c,%.o,$(C_FILES)))

all: $(BIN)

fast:
	make all -j $(NUM_THR)

$(BIN): $(TARGET) $(O_FILES)
	mkdir -p $(dir $(BIN))
	$(CC) $(TARGET) $(O_FILES) $(CFLAGS) $(LFLAGS) -o $(BIN)

$(BUILD_DIR)/obj/%.o: ./%.c 
	mkdir -p $(dir $@)
	$(CC) $(LFLAGS) $(CFLAGS) -c $< -o $@

run:
	$(BIN)

clean:
	rm -rf $(O_FILES) $(BIN)

.PHONY: clean run 
